/** this is an autogenerated config file */
/** This file is used by the buil-system */
#ifndef __IGEL_H
#define __IGEL_H

#define IGEL_VERSION_MAJOR 0
#define IGEL_VERSION_MINOR 1
#define IGEL_VERSION_PATCH 2
#define IGEL_VERSION_EXTRA "-dev"
#define IGEL_VERSION "0.1.2-dev"

#endif /* end of include guard: IGELCONF_H */
