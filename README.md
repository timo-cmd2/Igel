# Igel

A dynamic compiled Lisp-dialect written in VHDL. Igel features intuitive Clojure'ish syntax as well as great FFI to C and VHDL.

More docs to follow up soon...

> Note: This project has no relation to the <a href="https://www.igel.de/">Igel technology</a>. It has just the same name.
